import { IUser } from './user.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentUser : IUser;
  constructor() { }

  login(username: string, password: string){
    this.currentUser = {
      id: 1,
      firstName: "John",
      lastName: "papa",
      userName: username
    }
  }

  updateUserDetails(firstName: string, lastName: string){
    this.currentUser.firstName = firstName;
    this.currentUser.lastName = lastName;
  }

  isAuthenticated(){
    return !!this.currentUser;
  }
}
