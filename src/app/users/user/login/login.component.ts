import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'src/app/common/toastr.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userName: string;
  password: string;
  
  constructor(private authService : AuthService,
              private route: Router,
              private toast: ToastrService) { }

  ngOnInit(): void {
  }

  login(data){
    console.log(data);
    this.authService.login(data.userName, data.password);
    this.toast.success("loggedIn Successfully!!")
    this.route.navigate(['/events']);
  }

}
