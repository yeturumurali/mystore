import { AuthService } from './../user/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profileForm : FormGroup;

  constructor( private auth: AuthService,
                private route : Router) { }

  ngOnInit(): void {

    let firstName = new FormControl(this.auth.currentUser.firstName, Validators.required);
    let lastName = new FormControl(this.auth.currentUser.lastName, Validators.required);

    this.profileForm = new FormGroup({
      firstName  : firstName,
      lastName : lastName 
    });
  }

  updateProfile(formData){
    this.auth.updateUserDetails(formData.firstName, formData.lastName);
    this.route.navigate(['/events']);
  } 

  cancel(){
    this.route.navigate(['/events']);
  }

}
