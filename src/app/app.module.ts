
import { EventDetailsComponent } from './events/event-details/event-details.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { JQ_TOKEN } from './common/jQuery.service'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventslistComponent } from './events/eventslist/eventslist.component';
import { EventthumbnailComponent } from './events/eventthumbnail/eventthumbnail.component';
import { NavbarComponent } from './nav/navbar/navbar.component';
import { CreateeventComponent } from './events/createevent/createevent.component';
import { NotfoundComponent } from './errors/notfound/notfound.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateSessionComponent } from './events/event-details/create-session/create-session.component';
import { SessionListComponent } from './events/event-details/session-list/session-list.component';
import { CollapsableWellComponent } from './common/collapsable-well/collapsable-well.component';
import { SessionDurationPipe } from './events/shared/session-duration.pipe';

let jQuery = Window['$'];

@NgModule({
  declarations: [
    AppComponent,
    EventslistComponent,
    EventthumbnailComponent,
    NavbarComponent,
    EventDetailsComponent,
    CreateeventComponent,
    NotfoundComponent,
    CreateSessionComponent,
    SessionListComponent,
    CollapsableWellComponent,
    SessionDurationPipe 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    // {
    //   provide: 'canDeactivateCreateEvent',
    //   useClass: checkDirtyState
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function checkDirtyState() {
  return false;
}
