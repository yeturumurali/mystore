import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-collapsable-well',
  templateUrl: './collapsable-well.component.html',
  styleUrls: ['./collapsable-well.component.css']
})
export class CollapsableWellComponent implements OnInit {

  visible: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  toggleContent(){
    this.visible = !this.visible;
  }

}
