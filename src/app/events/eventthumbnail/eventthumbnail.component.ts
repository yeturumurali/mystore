import { IEvent } from './../shared/event.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-eventthumbnail',
  templateUrl: './eventthumbnail.component.html',
  styleUrls: ['./eventthumbnail.component.css']
})
export class EventthumbnailComponent implements OnInit {

  @Input() event: IEvent
  //@Output() eventClick = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
