import { EventService } from './../shared/event.service';
import { Component, OnInit } from '@angular/core';
import { IEvent } from '../shared/event.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-createevent',
  templateUrl: './createevent.component.html',
  styleUrls: ['./createevent.component.css']
})
export class CreateeventComponent implements OnInit {

  newEvent : IEvent ;

  constructor(private route : Router,
              private eventService : EventService) { }

  ngOnInit(): void {

    this.newEvent = {
      id: 895,
      name: "ng-Spectacular",
      date: new Date('02/05/2045'),
      time: '10.00 am',
      price: 899.99,
      location:{
        address: '456 happy st',
        city: 'Felicity',
        country:'angularisthan',
      },
      sessions:[],
      onlineUrl: '',
      imageUrl:'http://ngspec.com/logo.png' 
    }
  }

  saveEvent(eventData){
    console.log(eventData);
    this.eventService.saveEvent(eventData);
    this.route.navigate(['/events']);
  }

  cancel(){
    this.route.navigate(['/events']);
  } 

}
