import { IEvent } from './../shared/event.model';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from './../../common/toastr.service';
import { EventService } from './../shared/event.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eventslist',
  templateUrl: './eventslist.component.html',
  styleUrls: ['./eventslist.component.css']
})
export class EventslistComponent implements OnInit {

  events:IEvent[]

  constructor(private eventService : EventService,
              private toastrService : ToastrService,
              private route : ActivatedRoute  ) { 
    
  }

  ngOnInit(): void {
    this.events = this.route.snapshot.data['events']
  }

  ontoasterClick(eventName){
    this.toastrService.success(eventName);
  }

} 
