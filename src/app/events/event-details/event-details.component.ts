import { IEvent, ISession } from './../shared/event.model';
import { Component, OnInit } from '@angular/core';
import { EventService } from '../shared/event.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {

  event: IEvent;
  addMode;
  filterby : string = 'all';
  sortby : string = 'votes';
  constructor( private eventService : EventService,
               private route : ActivatedRoute,
               private navigateRoute : Router ) { }

  ngOnInit(): void {
    this.event = this.eventService.getEvent( + this.route.snapshot.params['id']);
  }

  addSessions(){
    this.addMode = true;
  }

  addSessiontiEvent(sessionData : ISession){
    const nextId = Math.max.apply(null, this.event.sessions.map(s=>s.id));
    sessionData.id = nextId + 1;
    this.event.sessions.push(sessionData);
    this.eventService.updateEvent(this.event);
    // this.navigateRoute.navigate(['/events/', + this.route.snapshot.params['id']])
    this.addMode = false;
  }

}
 