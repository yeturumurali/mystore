import { EventService } from './shared/event.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventRouteActivatorGuard implements CanActivate {  

  constructor(private eventService : EventService,
              private route: Router) {
    
  } 

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      const eventExists = !! this.eventService.getEvent(+ next.params['id']);
      console.log(eventExists);
      if(!eventExists) this.route.navigate(['/error']);

      return eventExists;
  } 
  
}
