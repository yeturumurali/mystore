import { CreateSessionComponent } from './events/event-details/create-session/create-session.component';
import { UserModule } from './users/user/user.module';
import { EventListResolver } from './events/event-list-resolver.service';
import { EventRouteActivatorGuard } from './events/event-route-activator.guard';
import { NotfoundComponent } from './errors/notfound/notfound.component';
import { CreateeventComponent } from './events/createevent/createevent.component';
import { EventDetailsComponent } from './events/event-details/event-details.component';
import { EventslistComponent } from './events/eventslist/eventslist.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 

const routes: Routes = [
  {path:"events/new", component:CreateeventComponent},
  {path:"error", component:NotfoundComponent},
  {path:"events", component:EventslistComponent, resolve: {events : EventListResolver} },
  {path:"events/session/new", component: CreateSessionComponent},
  {path:"events/:id", component:EventDetailsComponent, canActivate:[EventRouteActivatorGuard]},
  {path:"", redirectTo:"/events", pathMatch:"full"},
  {path:"user", loadChildren : ()=> UserModule}
]; 
 
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
}) 
export class AppRoutingModule { }
 