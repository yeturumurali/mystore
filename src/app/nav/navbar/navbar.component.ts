import { ISession } from './../../events/shared/event.model';
import { EventService } from './../../events/shared/event.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/users/user/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  searchTerm : string ="";
  foundSessions : ISession[] = [];

  constructor(public auth : AuthService,
              private eventService : EventService) { }

  ngOnInit(): void {
  }

  searchSessions(term){
    this.eventService.searchSessions(term).subscribe(sessions=>{
      this.foundSessions = sessions;
      console.log(this.foundSessions);
    })
  }
}
